# Wordle

Simple script that plays Wordle using a strateg for the "not hard" mode.

The strategy picks words that aim to eliminate the maximum number of other words, i.e. containing letters that, when they are not in the answer, together eliminate the largest number of words.

Typically in a run of 500 games (selecting a 5-letter answer at random from 5,000 words of the same length), the game will lose fewer than 10 games (i.e.  guess the target word in 6 or fewer steps)

The game reports it state full state after every move and at the end some stats on what a series of runs resulted in.

The `main()`:
```python
    w = Wordle()
    w.stats(size=5, games=500)
```
Simply creates the game and runs 500 games with 5-letter word-length. If you just want to see how it does on a specific word:
```python
    w = Wordle()
    w.play('adobe')
    w.play('longer')
    w.play('tiny')
```
