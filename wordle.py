import re
from random import shuffle
from pathlib import Path
from urllib.request import urlopen
from collections import defaultdict, Counter
from functools import reduce

word_collections = {
    'google 10k': {
        'description': 'The 10,000 most common words in English without swears, based on Google trillion word corpus.',
        'source': 'https://raw.githubusercontent.com/'
                  'first20hours/google-10000-english/master/google-10000-english-no-swears.txt',
        'filter': re.compile(r'.*'),
        'count': 10000
    },
    'norvig ngrams': {
        'description': 'Peter Norvig\'s compilation of the 1/3 million most frequent English words '
                       'from the Google trillion word corpus.',
        'source': 'https://norvig.com/ngrams/count_1w.txt',
        'filter': re.compile(r'(\w+)'),
        'count': 100000
    },
}


class Wordle:
    def __init__(self, word_collection='norvig ngrams'):
        self.state = {}
        self.all_words = []
        self.word_collection = word_collections[word_collection]
        self.load_all_words()
        self.options = []
        self.possible_guesses = []
        self.common_chars = {}
        self.i_eliminated_by = {}
        # the cache is only here to avoid redoing previous computations in best guess
        self.cache = {}

    def load_all_words(self):
        word_cache = 'word_cache.txt'

        if not Path(word_cache).is_file():
            with urlopen(self.word_collection['source']) as r:
                with open(word_cache, 'wb') as f:
                    f.write(r.read())

        with open(word_cache) as f:
            self.all_words = []
            i_all_words = iter(f.read().splitlines())
            while len(self.all_words) < self.word_collection['count']:
                w = next(i_all_words)
                if m := re.match(self.word_collection['filter'], w):
                    self.all_words.append(m.group(1))

    def update_options(self):
        """
        Updates options based on state. Only keeps options that don't have letters that are not in the word, have
        letters that are in the word, and have letters that have to be in a specific place, in that place
        :return:
        """
        self.options = [w for w in self.options if all(
            # check that it doesn't have letters that are not in the word
            ch not in w if not self.state[ch]
            # check that it has letters that are in the word somewhere
            else ch in w if not isinstance(self.state[ch], list)
            # if a position for a letter is known, check that it is in all known positions
            else all(w[p] == ch for p in self.state[ch])
            for ch in self.state
        )]

    def make_move(self, guess: str, answer: str):
        """
        updates the game state and words that are still an option
        :param guess: the move
        :param answer: the actual answer we need to guess
        :return: None
        """
        assert len(guess) == len(answer)

        for p, ch in enumerate(guess):
            if ch in answer:
                if answer[p] == ch:
                    if ch not in self.state or not isinstance(self.state[ch], list):
                        self.state[ch] = []
                    if p not in self.state[ch]:
                        self.state[ch].append(p)
                else:
                    if ch not in self.state or not isinstance(self.state[ch], list):
                        self.state[ch] = True
            else:
                self.state[ch] = False

        self.update_options()

    def best_guess(self) -> (str, int):
        """
        The best guess is the word that introduces the most new letters that are most common in words that have the
        known letters (in the right place if their position is known)
        :return: a word and its score
        """
        best = 0
        words = []
        for word in self.possible_guesses:
            # some extra plumbing here, which avoids recomputing a score for the same options and word,
            # which would result in the exact same score (and is a very slow operation)
            options_key = (tuple(self.options), word)
            if options_key in self.cache:
                score = self.cache[options_key]
            else:
                score = len(list(reduce(set.union, [self.i_eliminated_by[ch]
                                                    for ch in word
                                                    if ch in self.i_eliminated_by and ch not in self.state], set())))
                self.cache[options_key] = score
            if score >= best:
                if score > best:
                    best = score
                    words = [word]
                else:
                    words.append(word)

        if len(words) == 1:
            return words[0], best
        else:
            options_words = set(words).intersection(set(self.options))
            if options_words:
                words = options_words
            return sorted(words, key=lambda w: self.possible_guesses.index(w))[0], best

    def update_common_chars(self):
        """
        updates common_chars, which is a dictionary of char: int, indicating how many words would be eliminated for
        each character, if a word with that character was picked and ch is not in the answer.
        :return:
        """
        have_chars = defaultdict(int)
        for w in self.options:
            for ch in set(w):
                have_chars[ch] += 1

        self.common_chars = dict(sorted([(ch, n) for ch, n in have_chars.items()], key=lambda t: t[1], reverse=True))

    def update_eliminated_words(self):
        """
        Updates i_eliminated_by, which is a dictionary of char: list[int], which indicated words at what indices into
        options would be eliminated by picking a word that has ch, if that character is not in the answer.
        :return: None
        """
        chars = reduce(set.union, [set(w) for w in self.options])
        self.i_eliminated_by = {ch: set(n for n, w in enumerate(self.options) if ch in w) for ch in chars}

    def play(self, word, top=5000):
        """
        Play a game of Wordle
        :param word: the word to guess (from words of the same length)
        :param top: what part of the available words to use, i.e. the top X
        :return: the number of attempts that were needed to guess the word
        """
        print(f'Playing for {word}')
        self.state = {}

        n = len(word)
        n_words = [w for w in self.all_words if len(w) == n]
        print(f'There are {len(n_words)} words of length {n} in the collection.')
        self.possible_guesses = n_words[:top]
        *self.options, = self.possible_guesses
        print(f'Using only the top {top} as options, the 5 least common words are {self.options[-5:]}')

        if word not in self.options:
            print(f'The play word "{word}" is not among the options!')
            return -1

        tries = 0
        while True:
            self.update_common_chars()
            self.update_eliminated_words()
            print(f'These characters appear in the {len(self.options)} options, and in how many:\n{self.common_chars}')

            print(f'Making the best guess: {(best := self.best_guess())}')
            tries += 1
            self.make_move(best[0], word)
            if best[0] == word:
                print(f'Guessed it after {tries} tries: {best[0]}')
                return tries
            if best[0] in self.options:
                print(f'"{best[0]}" would still be possible, but just eliminated')
                self.options.remove(best[0])
            self.possible_guesses.remove(best[0])
            print(f'What we now know: {self.state}')
            print(f'There are {len(self.options)} options left, the most common 5: {self.options[:5]}')

    def stats(self, size, top=5000, games=500):
        game_words = [w for w in self.all_words if len(w) == size][:top]
        shuffle(game_words)
        scores = [self.play(word, top=top) for word in game_words[:games]]
        average = sum(scores) / games
        print(f'After {games} different random games, the average number of tries was {average}.')
        print(f'The minimum score was {min(scores)}, and the maximum {max(scores)}')
        print(f'The distribution: {Counter(scores)}')
        print(f'Losing words were: {[w for w, s in zip(game_words[:games], scores) if s > 6]}')


def main():
    w = Wordle()
    w.stats(size=5, games=500)


if __name__ == '__main__':
    main()
